DATABASES = {
   'default': {
      'ENGINE': 'django.db.backends.postgresql_psycopg2',
      'NAME': 'prueba', # Or path to database file if using sqlite3.
      'USER': 'pruebausr', # Not used with sqlite3.
      'PASSWORD': 'pruebausr', # Not used with sqlite3.
      'HOST': 'localhost', # Set to empty string for localhost. Not used with sqlite3.
      'PORT': '', # Set to empty string for default. Not used with sqlite3.
   }
}

FIXTURE_DIRS =["files/fixtures/"]
