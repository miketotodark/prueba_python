from django.urls import path
from . import views

urlpatterns =[
    path('movies_by_actor/', views.movies_by_actor, name='movies_by_actor'),
    path('movies_by_director/', views.movies_by_director, name='movies_by_director'),
    path('get_directors/', views.get_directors, name='get_directors'),
    path('get_actors/', views.get_actors, name='get_actors'),
    path('genres_movies/', views.genres_movies, name='genres_movies')
]
