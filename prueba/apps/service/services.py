# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import pandas as pd
import numpy as np
import json


df = pd.read_csv('files/movie_metadata.csv', encoding='utf-8') #load csv file
df.rename(columns= df.iloc[0]) # make first row headers




#datafram with directors data
directors =pd.DataFrame( {"name":df.director_name, "likes_facebook": df.director_facebook_likes} )
directors = directors.drop_duplicates() #delete data duplicated
directors = directors.dropna() #delete data NA
directors.index = np.arange(1, len(directors) + 1) #generatee index from 1 to len of dataframe

#datafram with actors data
actors = pd.concat([pd.DataFrame({'name':df.actor_1_name, 'likes_facebook':df.actor_1_facebook_likes}),
    pd.DataFrame({'name':df.actor_2_name, 'likes_facebook':df.actor_2_facebook_likes}),
    pd.DataFrame({'name':df.actor_3_name, 'likes_facebook':df.actor_3_facebook_likes})])
actors = actors.drop_duplicates()
actors = actors.dropna()
actors.index = np.arange(1, len(actors) + 1)

reviews = pd.DataFrame({'num_criticas':df.num_critic_for_reviews,
    'num_voted_user':df.num_voted_users,
    'num_user_reviews': df.num_user_for_reviews})
reviews.index = np.arange(1, len(reviews) + 1)

genres = pd.DataFrame({'name':df.genres.str.split('|').tolist()})
genres=genres.name.apply(pd.Series)\
    .melt(value_name='name')\
    .drop (['variable'], axis=1)\
    .dropna()
genres = genres.drop_duplicates()
genres.index = np.arange(1, len(genres) + 1)

keywords = pd.DataFrame({'name':df.plot_keywords.str.split('|').tolist()})
keywords=keywords.name.apply(pd.Series)\
    .melt(value_name='name')\
    .drop (['variable'], axis=1)\
    .dropna()
keywords = keywords.drop_duplicates()
keywords.index = np.arange(1, len(keywords) + 1)

for i in df:
    df.loc[df[i].isnull(),i] = '' if df[i].dtype.kind =='O' else 0
fk_director,fk_review, fk_actors, fk_geners, fk_keywords=[],[],[],[],[]
for index, row in df.iterrows():
    d =directors[(row.director_name!='')&(directors.name == row.director_name)]
    fk_director.append(int(d.index[0]) if not d.empty else 0)
    fk_review.append(reviews[(reviews.num_criticas == row.num_critic_for_reviews) &
    (reviews.num_voted_user == row.num_voted_users) &
    (reviews.num_user_reviews == row.num_user_for_reviews)].index[0])
    a1=actors[(row.actor_1_name!='')&(actors.name == row.actor_1_name)]
    a2=actors[(row.actor_2_name!='')&(actors.name == row.actor_2_name)]
    a3=actors[(row.actor_3_name!='')&(actors.name == row.actor_3_name)]
    fk_actors.append([ a for a in [
        int(a1.index[0]) if not a1.empty else 0,
        int(a2.index[0]) if not a2.empty else 0,
        int(a3.index[0]) if not a3.empty else 0] if a !=0
    ])
    g =[]
    for gen in row.genres.split('|'):
        g.append(int(genres[genres.name==gen].index[0]))
    fk_geners.append(g)
    kw =[]
    for keyw in row.plot_keywords.split('|'):
        kw.append(int(keywords[keywords.name==keyw].index[0]))
    fk_keywords.append(kw)

movies = pd.DataFrame({'color': df.color, 'duration':df.duration, 'gross':df.gross,
    'title':df.movie_title, 'cast_total_fb':df.cast_total_facebook_likes,
    'facenumber':df.facenumber_in_poster, 'link':df.movie_imdb_link,
    'language':df.language, 'country':df.country, 'content_rating':df.content_rating,
    'budget':df.budget,  'year':df.title_year, 'imdb_score':df.imdb_score,
    'aspect_ratio':df.aspect_ratio, 'likes_facebook': df.movie_facebook_likes,
    'fk_actors':fk_actors, 'fk_reviews':fk_review, 'fk_geners':fk_geners,
    'fk_keywords':fk_keywords, 'fk_director':fk_director
    })
movies.index = np.arange(1, len(movies) + 1)

def writoToFile(dataframe, file, model):
    with open(file, 'w') as outfile:
        json.dump(passToarray(dataframe, model), outfile)

writoToFile(actors, "fixture1.json", "Actores")
writoToFile(directors, "fixture2.json", "Director")
writoToFile(reviews, "fixture3.json", "Reviews")
writoToFile(genres, "fixture4.json", "Generos")
writoToFile(keywords, "fixture5.json", "Keywords")
writoToFile(movies, "fixture6.json", "Movies")
