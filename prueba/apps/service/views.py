from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import *
from .serializers import *
from django.db.models import Count
# Create your views here.
@api_view(['POST'])
def movies_by_actor(request):
    serializer = Actor_serializer(data = request.data)
    if serializer.is_valid():
        movies = Movies.objects.filter(fk_actors=serializer.data['actor_id']).values('id', 'title','year',
        'fk_actors__name')
        response = [movie for movie in movies]
        return Response({"data":response})
    else:
        return Response({'error': serializer.errors})

@api_view(['POST'])
def movies_by_director(request):
    serializer = Director_serializer(data = request.data)
    if serializer.is_valid():
        movies = Movies.objects.filter(fk_director=serializer.data['director_id']).values('id', 'title',
        'year','fk_director__name').order_by('year')
        response = [movie for movie in movies]
        return Response({"data":response})
    else:
        return Response({'error': serializer.errors})

@api_view(['POST'])
def genres_movies(request):
    genres = Movies.objects.all().values('fk_geners__name').annotate(movies=Count('id')).order_by('-movies')
    response = [genre for genre in genres]
    return Response({"data":response})

@api_view(['GET'])
def get_actors(request):
    actors = Actores.objects.all().values('id', 'name','likes_facebook').order_by('likes_facebook')
    response = [actor for actor in actors]
    return Response({"data":response})

@api_view(['GET'])
def get_directors(request):
    directors = Director.objects.all().values('id', 'name','likes_facebook')
    response = [director for director in directors]
    return Response({"data":response})
