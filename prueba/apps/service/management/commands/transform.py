from django.core.management.base import BaseCommand
import json
import pandas as pd
import numpy as np
import time


class Command(BaseCommand):
    def pass_to_array(self, dataframe, model):
        array=[]
        for index, row in dataframe.iterrows():
            temp = {}
            temp['model']="service."+model
            temp['pk'] = index
            temp['fields'] = row.to_dict()
            if 'fk_director' in temp['fields'] and temp['fields']['fk_director'] ==0:
                temp['fields'].pop('fk_director', None)
            array.append(temp)
        return array

    def writoToFile(self, dataframe, file, model):
        with open(file, 'w') as outfile:
            json.dump(self.pass_to_array(dataframe, model), outfile)
    def convert_to_JSON(self, dataf):

        df =dataf

        #datafram with directors data
        directors =pd.DataFrame( {"name":df.director_name, "likes_facebook": df.director_facebook_likes} )
        directors = directors.drop_duplicates() #delete data duplicated
        directors = directors.dropna() #delete data NA
        directors.index = np.arange(1, len(directors) + 1) #generatee index from 1 to len of dataframe

        #datafram with actors data
        actors = pd.concat([pd.DataFrame({'name':df.actor_1_name, 'likes_facebook':df.actor_1_facebook_likes}),
            pd.DataFrame({'name':df.actor_2_name, 'likes_facebook':df.actor_2_facebook_likes}),
            pd.DataFrame({'name':df.actor_3_name, 'likes_facebook':df.actor_3_facebook_likes})]) #join the actor's data
        actors = actors.drop_duplicates()
        actors = actors.dropna()
        actors.index = np.arange(1, len(actors) + 1)

        reviews = pd.DataFrame({'num_criticas':df.num_critic_for_reviews,
            'num_voted_user':df.num_voted_users,
            'num_user_reviews': df.num_user_for_reviews}).fillna(0)
        reviews.index = np.arange(1, len(reviews) + 1)
        genres = pd.DataFrame({'name':df.genres.str.split('|').tolist()}) #split the information
        genres=genres.name.apply(pd.Series)\
            .melt(value_name='name').drop(['variable'], axis=1).dropna()
        genres = genres.drop_duplicates()
        genres.index = np.arange(1, len(genres) + 1)

        keywords = pd.DataFrame({'name':df.plot_keywords.str.split('|').tolist()})
        keywords=keywords.name.apply(pd.Series)\
            .melt(value_name='name')\
            .drop (['variable'], axis=1)\
            .dropna()
        keywords = keywords.drop_duplicates()

        keywords.index = np.arange(1, len(keywords) + 1)
        for i in df:
            df.loc[df[i].isnull(),i] = '' if df[i].dtype.kind =='O' else 0 # fill the NaN data to "" if is a string or 0 if is number


        fk_director,fk_review, fk_actors, fk_geners, fk_keywords=[],[],[],[],[]
        for index, row in df.iterrows(): #itterate original dataframe to relate information
            d = directors[(row.director_name!='')&(directors.name == row.director_name)] # lookup for the index of the director in the directors data
            fk_director.append(int(d.index[0]) if not d.empty else 0)
            fk_review.append(reviews[(reviews.num_criticas == row.num_critic_for_reviews) &
            (reviews.num_voted_user == row.num_voted_users) &
            (reviews.num_user_reviews == row.num_user_for_reviews)].index[0])
            a1=actors[(row.actor_1_name!='')&(actors.name == row.actor_1_name)]
            a2=actors[(row.actor_2_name!='')&(actors.name == row.actor_2_name)]
            a3=actors[(row.actor_3_name!='')&(actors.name == row.actor_3_name)]
            fk_actors.append([ a for a in [
                int(a1.index[0]) if not a1.empty else 0,
                int(a2.index[0]) if not a2.empty else 0,
                int(a3.index[0]) if not a3.empty else 0] if a !=0
            ])
            g =[]
            for gen in row.genres.split('|'):
                g.append(int(genres[genres.name==gen].index[0]))
            fk_geners.append(g)
            kw =[]
            for keyw in row.plot_keywords.split('|'):
                t=keywords[(keyw!='')& (keywords.name==keyw)]
                if not t.empty:
                    kw.append(int(t.index[0]))
            fk_keywords.append(kw)

        movies = pd.DataFrame({'color': df.color, 'duration':df.duration, 'gross':df.gross,
            'title':df.movie_title, 'cast_total_fb':df.cast_total_facebook_likes,
            'facenumber':df.facenumber_in_poster, 'link':df.movie_imdb_link,
            'language':df.language, 'country':df.country, 'content_rating':df.content_rating,
            'budget':df.budget,  'year':df.title_year, 'imdb_score':df.imdb_score,
            'aspect_ratio':df.aspect_ratio, 'likes_facebook': df.movie_facebook_likes,
            'fk_actors':fk_actors, 'fk_reviews':fk_review, 'fk_geners':fk_geners,
            'fk_keywords':fk_keywords, 'fk_director':fk_director
            })
        movies.index = np.arange(1, len(movies) + 1)

        self.writoToFile(actors, "files/fixtures/fixture1.json", "Actores")
        self.writoToFile(directors, "files/fixtures/fixture2.json", "Director")
        self.writoToFile(reviews, "files/fixtures/fixture3.json", "Reviews")
        self.writoToFile(genres, "files/fixtures/fixture4.json", "Generos")
        self.writoToFile(keywords, "files/fixtures/fixture5.json", "Keywords")
        self.writoToFile(movies, "files/fixtures/fixture6.json", "Movies")


    def add_arguments(self, parser):
        parser.add_argument("path", type =str)

    def handle(self, *args, **kwargs):
        start_time = time.time()
        path = kwargs['path']
        df = pd.read_csv(path, encoding='utf-8') #load csv file
        df.rename(columns= df.iloc[0]) # make first row headers
        self.convert_to_JSON(df)
        print("--- Proceess time:  %s seconds ---" % (time.time() - start_time))
