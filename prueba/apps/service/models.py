from django.db import models

# Create your models here.

class Director(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 200)
    likes_facebook = models.IntegerField(default = 0)
    def __str__(self):
        return self.name + " fb likes: "+ str(self.likes_facebook)

class Actores(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 200)
    likes_facebook = models.IntegerField(default = 0)
    def __str__(self):
        return self.name + " fb likes: "+ str(self.likes_facebook)

class Generos(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 200)
    def __str__(self):
        return self.name

class Keywords(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 200)
    def __str__(self):
        return self.name

class Reviews(models.Model):
    id = models.AutoField(primary_key = True)
    num_criticas = models.IntegerField(default=0)
    num_voted_user = models.IntegerField(default=0)
    num_user_reviews = models.IntegerField(default=0)
    def __str__(self):
        return "critics: "+str(self.num_criticas)+ " voted user: "+ str(self.num_voted_user)+" users_reviews: "+str(self.num_user_reviews)

class Movies(models.Model):
    id = models.AutoField(primary_key = True)
    color = models.CharField(max_length = 200)
    gross = models.BigIntegerField(default=0)
    title = models.CharField(max_length = 200)
    link = models.CharField(max_length = 200)
    language = models.CharField(max_length = 200)
    country = models.CharField(max_length = 200)
    content_rating = models.CharField(max_length = 200)
    duration = models.IntegerField(default=0)
    cast_total_fb = models.BigIntegerField(default=0)
    facenumber = models.IntegerField(default=0)
    budget = models.BigIntegerField(default=0)
    year = models.IntegerField(default=0)
    likes_facebook = models.IntegerField(default=0)
    imdb_score = models.DecimalField(max_digits=6, decimal_places=4)
    aspect_ratio = models.DecimalField(max_digits=6, decimal_places=4)
    fk_director = models.ForeignKey(Director, on_delete=models.CASCADE, null=True)
    fk_reviews = models.ForeignKey(Reviews, on_delete=models.CASCADE)
    fk_actors = models.ManyToManyField(Actores)
    fk_geners = models.ManyToManyField(Generos)
    fk_keywords = models.ManyToManyField(Keywords)

    def __str__(self):
        return self.title


# class MovieActor (mo)
