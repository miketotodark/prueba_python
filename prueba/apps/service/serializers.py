from rest_framework import serializers

class Director_serializer(serializers.Serializer):
    director_id = serializers.IntegerField()

class Actor_serializer(serializers.Serializer):
    actor_id = serializers.IntegerField()
