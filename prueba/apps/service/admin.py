from django.contrib import admin
from .models import Movies, Director
from django.db.models import Count, Sum
# Register your models here.


class RaisedMostMoneyFilter(admin.SimpleListFilter):
    title= ('Raised money')
    parameter_name  = "gross"
    def lookups(self, request, model_admin):
        return (("most", ("most")),("least", ("least")))
    def queryset(self, request, queryset):
        response =queryset.all()
        if self.value() =='most':
            response = response.order_by('-gross')
        elif self.value() =='least':
            response = response.order_by('gross')
        return response
class SpentMostMoneyFilter(admin.SimpleListFilter):
    title= ('Spent money')
    parameter_name  = "budget"
    def lookups(self, request, model_admin):
        return (("most", ("most")),("least", ("least")))
    def queryset(self, request, queryset):
        response =queryset.all()
        if self.value() =='most':
            response = response.order_by('-budget')
        elif self.value() =='least':
            response = response.order_by('budget')
        return response
class DirectorBestReputationFilter(admin.SimpleListFilter):
    title= ('Best Resputation')
    parameter_name  = "likes_facebook"
    def lookups(self, request, model_admin):
        return (("best", ("Best Resputation")),)
    def queryset(self, request, qs):
        response =qs.all()
        if self.value() == "best":
            response = qs.all().order_by('-likes_facebook')
            return response
        return response
@admin.register(Movies)
class MoviesAdmin (admin.ModelAdmin):
    model = Movies
    exclude = ('id', )
    list_display=['title', 'gross', 'budget']
    filter_horizontal = ('fk_actors','fk_geners', 'fk_keywords')
    empty_value_display = '-empty-'
    list_filter  = (RaisedMostMoneyFilter, SpentMostMoneyFilter)

@admin.register(Director)
class DirectorAdmin (admin.ModelAdmin):
    model = Director
    exclude = ('id', )
    list_display=['name', 'likes_facebook']
    empty_value_display = '-empty-'
    list_filter  = (DirectorBestReputationFilter, )
